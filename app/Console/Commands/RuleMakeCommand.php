<?php

namespace App\Console\Commands;

use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'make:rule')]
class RuleMakeCommand extends \Illuminate\Foundation\Console\RuleMakeCommand
{
    /**
     * @param $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace): string
    {
        return 'App\Http\v1\Rules';
    }
}
