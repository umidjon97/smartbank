<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function render($request, Throwable $e): Response|JsonResponse|RedirectResponse|\Symfony\Component\HttpFoundation\Response
    {
        $routePrefix = Route::current()?->getPrefix();
        $contentType = $request->header('content-type');

        if ($contentType === 'application/json' || (!empty($routePrefix) && $routePrefix === 'api')) {

            if (!($e instanceof ValidationException)) {

                /**
                 * @var HttpException $e
                 */
                if ($this->isHttpException($e)) {
                    $this->statusCode = $e->getStatusCode();
                }

                if ($e instanceof ModelNotFoundException) {
                    $this->statusCode = 404;
                }

                return response()->json([
                    'status'  => false,
                    'message' => $e->getMessage(),
                    'code'    => $e->getCode(),
                    'data'    => []
                ], !empty($this->statusCode) ? $this->statusCode : 500);
            }
        }

        return parent::render($request, $e);
    }

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
