<?php

namespace App\Exceptions;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Lang;

trait ValidationException
{
    protected static array $messages = [];

    /**
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator): void
    {
        $this->responseFormat($validator->getMessageBag()->getMessages());

        throw new HttpResponseException(response()->json([
            'message' => Lang::get('Validation Error'),
            'errors'  => self::$messages,
            'code'    => 422
        ], 422));
    }

    /**
     * @param array $errors
     * @return void
     */
    protected function responseFormat(array $errors): void
    {
        foreach ($errors as $k => $error) {
            foreach ($error as $item) {
                self::$messages[] = [
                    'field'   => $k,
                    'message' => $item
                ];
            }
        }
    }
}
