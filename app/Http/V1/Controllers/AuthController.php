<?php

namespace App\Http\V1\Controllers;

use App\Http\v1\Request\AuthRequest;
use App\Http\V1\Services\Auth\AuthInterface;
use App\Http\V1\Services\Responses\ResponseInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    protected AuthInterface $authService;
    protected ResponseInterface $responseService;

    public function __construct(
        AuthInterface     $authService,
        ResponseInterface $responseService
    )
    {
        $this->authService = $authService;
        $this->responseService = $responseService;
    }

    /**
     * @param AuthRequest $authRequest
     * @return JsonResponse
     */
    public function login(AuthRequest $authRequest): JsonResponse
    {
        $this->authService->login($authRequest);

        $result = [
            'access_token' => $this->authService->getAccessToken(),
            'type'         => 'bearer'
        ];

        return $this->responseService->success(
            Lang::get('Login ok'),
            $result
        );
    }

    public function refreshToken()
    {

    }

    public function logout()
    {

    }
}
