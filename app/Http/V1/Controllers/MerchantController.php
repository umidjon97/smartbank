<?php

namespace App\Http\V1\Controllers;

use App\Http\v1\Request\Filter\MerchantFilterRequest;
use App\Http\V1\Request\MerchantRequest;
use App\Http\V1\Services\Merchants\MerchantInterface;
use App\Http\V1\Services\Responses\ResponseInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;

class MerchantController extends Controller
{
    protected MerchantInterface $merchantService;
    protected ResponseInterface $responseService;

    public function __construct(
        MerchantInterface $merchantService,
        ResponseInterface $responseService
    )
    {
        $this->merchantService = $merchantService;
        $this->responseService = $responseService;
    }

    /**
     * @param MerchantFilterRequest $filterRequest
     * @return LengthAwarePaginator
     */
    public function index(MerchantFilterRequest $filterRequest): LengthAwarePaginator
    {
        return $this->merchantService->list($filterRequest);
    }

    /**
     * @param MerchantRequest $merchantRequest
     * @return JsonResponse
     */
    public function store(MerchantRequest $merchantRequest): JsonResponse
    {
        $result = ['id' => $this->merchantService->save($merchantRequest)];

        return $this->responseService
            ->success(
                Lang::get('Successful saved!'),
                $result
            );
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        return $this->responseService
            ->success(
                Lang::get('Merchant Detail Info'),
                $this->merchantService->show($id)
            );
    }

    /**
     * @param MerchantRequest $merchantRequest
     * @param string $id
     * @return JsonResponse
     */
    public function update(MerchantRequest $merchantRequest, string $id): JsonResponse
    {
        $result = ['id' => $this->merchantService->update($merchantRequest, $id)];

        return $this->responseService
            ->success(
                Lang::get('Successful saved!'),
                $result
            );
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function destroy(string $id): JsonResponse
    {
        $this->merchantService->delete($id);

        return $this->responseService->success(Lang::get('Successful removed!'));
    }
}
