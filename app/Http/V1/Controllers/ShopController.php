<?php

namespace App\Http\V1\Controllers;

use App\Http\v1\Request\AddressRequest;
use App\Http\v1\Request\ShopRequest;
use App\Http\V1\Services\Responses\ResponseInterface;
use App\Http\V1\Services\Shops\ShopInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Lang;

class ShopController extends Controller
{
    protected ResponseInterface $responseService;
    protected ShopInterface $shopService;

    public function __construct(
        ResponseInterface $responseService,
        ShopInterface     $shopService
    )
    {
        $this->responseService = $responseService;
        $this->shopService = $shopService;
    }

    /**
     * @param ShopRequest $shopRequest
     * @param int $merchant_id
     * @return JsonResponse
     */
    public function store(ShopRequest $shopRequest, int $merchant_id): JsonResponse
    {
        $result = ['id' => $this->shopService->save($shopRequest, $merchant_id)];

        return $this->responseService->success(
            Lang::get('Shop assign to merchant!'),
            $result
        );
    }

    /**
     * @param ShopRequest $shopRequest
     * @param int $merchant_id
     * @param int $shop_id
     * @return JsonResponse
     */
    public function update(ShopRequest $shopRequest, int $merchant_id, int $shop_id): JsonResponse
    {
        $result = ['id' => $this->shopService->update($shopRequest, $merchant_id, $shop_id)];

        return $this->responseService->success(
            Lang::get('Shop assign to merchant!'),
            $result
        );
    }

    public function nearestAddress(AddressRequest $request)
    {
        return $this->shopService->getNearestAddress($request);
    }

    /**
     * @param int $merchant_id
     * @param int $shop_id
     * @return JsonResponse
     */
    public function destroy(int $merchant_id, int $shop_id): JsonResponse
    {
        $this->shopService->delete($merchant_id, $shop_id);
        return $this->responseService->success(Lang::get('Merchant shop removed!'));
    }
}
