<?php

namespace App\Http\V1\Controllers;

use App\Http\v1\Request\UserRequest;
use App\Http\V1\Services\Responses\ResponseInterface;
use App\Http\V1\Services\Users\UserInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    protected ResponseInterface $responseService;
    protected UserInterface $userService;

    public function __construct(
        ResponseInterface $responseService,
        UserInterface     $userService
    )
    {
        $this->responseService = $responseService;
        $this->userService = $userService;
    }

    /**
     * @param UserRequest $userRequest
     * @return JsonResponse
     */
    public function store(UserRequest $userRequest): JsonResponse
    {
        $result = ['id' => $this->userService->save($userRequest)];
        return $this->responseService->success(__('User saved!'), $result);
    }

    public function update(UserRequest $userRequest, int $id)
    {

    }

    public function destroy(int $id)
    {

    }
}
