<?php

namespace App\Http\V1\Helpers\Db;

use Illuminate\Support\Facades\DB;

class Transaction
{
    public function wrap(callable $function)
    {
        return DB::transaction($function);
    }
}
