<?php

namespace App\Http\V1\Helpers;

enum MerchantStatusHelper: string
{
    case _ACTIVE = 'active';
    case _BAN = 'ban';
}
