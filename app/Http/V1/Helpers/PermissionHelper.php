<?php

namespace App\Http\V1\Helpers;

enum PermissionHelper: string
{
    case _MODERATOR = 'moderator'; // without merchant
    case _MANAGER = 'manager'; // only one merchant user
}
