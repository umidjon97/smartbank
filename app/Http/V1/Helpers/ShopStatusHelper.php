<?php

namespace App\Http\V1\Helpers;

enum ShopStatusHelper: string
{
    case _ACTIVE = 'active';
    case _NO_ACTIVE = 'no_active';
}
