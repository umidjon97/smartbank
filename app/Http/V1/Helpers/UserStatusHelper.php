<?php

namespace App\Http\V1\Helpers;

enum UserStatusHelper: string
{
    case _ACTIVE = 'active';
    case _NO_ACTIVE = 'no_active';
    case _BAN = 'ban';
}
