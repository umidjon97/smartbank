<?php

namespace App\Http\v1\Middleware;

use App\Http\V1\Repository\UserRepository;
use App\Models\User;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthJwtMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            $jwtToken = JWTAuth::parseToken();

            if (!empty($id = (int)$jwtToken->getPayload()->get('sub'))) {

                /**
                 * @var User $user
                 */
                $user = App::make(UserRepository::class)
                    ->getById($id);

                if ($user->isBan()) {
                    abort(401, 'User is banned');
                }

                if (!$user->isActive()) {
                    abort(401, 'User is not active');
                }

                if (!$jwtToken->authenticate()) {
                    return $this->unauthorized(__('auth.User is not logged in.'));
                }
            } else {
                return $this->unauthorized(__('auth.Your token is invalid. Please, login again.'));
            }
        } catch (TokenExpiredException $e) {
            //Thrown if token has expired
            return $this->unauthorized(__('auth.Your token has expired. Please, login again.'));
        } catch (TokenInvalidException $e) {
            //Thrown if token invalid
            return $this->unauthorized(__('auth.Your token is invalid. Please, login again.'));
        } catch (JWTException $e) {
            //Thrown if token was not found in the request.
            return $this->unauthorized(__('auth.Please, attach a Bearer Token to your request'));
        }

        return $next($request);
    }

    /**
     * @param $message
     * @return JsonResponse
     */
    private function unauthorized($message = null): JsonResponse
    {
        return response()->json([
            'success' => false,
            'message' => $message ?: __('auth.You are unauthorized to access this resource'),
            'code'    => 401,
            'data'    => []
        ], 401);
    }
}
