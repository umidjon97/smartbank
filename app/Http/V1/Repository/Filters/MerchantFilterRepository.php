<?php

namespace App\Http\V1\Repository\Filters;

use App\Models\Merchant;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MerchantFilterRepository
{
    /**
     * @param Request $request
     * @param int $merchant_id
     * @return LengthAwarePaginator
     */
    public function filter(Request $request, int $merchant_id): LengthAwarePaginator
    {
        $query = Merchant::query();

        $query->with([
            'linkShops' => function (HasMany $q) {
                $q->select(DB::raw('merchant_id, shop_id, sh.address, sh.longitude, sh.latitude'))
                    ->join('shops as sh', 'sh.id', '=', 'shop_id');
            }
        ]);

        if (!empty($request->post('name'))) {
            $query->where('name', 'ilike', '%' . $request->post('name') . '%');
        }

        if (!empty($request->post('status'))) {
            $query->where('status', $request->post('status'));
        }

        if (!empty($request->post('registered_at'))) {
            $query->whereDate('registered_at', date('Y-m-d', strtotime($request->post('registered_at'))));
        }

        return $query->paginate(50000);
    }
}
