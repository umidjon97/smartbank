<?php

namespace App\Http\V1\Repository;

use App\Models\LinkMerchantShop;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Lang;

class LinkMerchantShopRepository
{
    /**
     * @param LinkMerchantShop $merchantShop
     * @return void
     */
    public function delete(LinkMerchantShop $merchantShop): void
    {
        $result = $merchantShop->delete();

        if ($result === false || is_null($result)) {
            throw new \RuntimeException(Lang::get('Link merchant shop dont remove'));
        }
    }

    /**
     * @param LinkMerchantShop $linkMerchantShop
     * @return void
     */
    public function save(LinkMerchantShop $linkMerchantShop)
    {
        if (!$linkMerchantShop->save()) {
            throw new \RuntimeException(Lang::get('Link Merchant Shop save error'));
        }
    }

    /**
     * @param int $merchant_id
     * @return Collection|array
     */
    public function getAllByMerchantId(int $merchant_id): Collection|array
    {
        return LinkMerchantShop::query()
            ->where('merchant_id', $merchant_id)
            ->get();
    }

    /**
     * @param int $shop_id
     * @return Collection|array
     */
    public function getByShopId(int $shop_id): Collection|array
    {
        return LinkMerchantShop::query()
            ->where('shop_id', $shop_id)
            ->get();
    }

    /**
     * @param int $merchant_id
     * @param int $shop_id
     * @return LinkMerchantShop|Builder
     */
    public function get(int $merchant_id, int $shop_id): LinkMerchantShop|Builder
    {
        return LinkMerchantShop::query()
            ->where('merchant_id', $merchant_id)
            ->where('shop_id', $shop_id)
            ->firstOrFail();
    }
}
