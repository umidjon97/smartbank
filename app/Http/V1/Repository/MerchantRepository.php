<?php

namespace App\Http\V1\Repository;

use App\Models\Merchant;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Lang;

class MerchantRepository
{
    /**
     * @param Merchant $merchant
     * @return void
     */
    public function save(Merchant $merchant): void
    {
        if (!$merchant->save()) {
            throw new \RuntimeException(Lang::get('Merchant Save Error'));
        }
    }

    /**
     * @param Merchant $merchant
     * @return void
     */
    public function delete(Merchant $merchant)
    {
        $result = $merchant->delete();

        if ($result === false || is_null($result)) {
            throw new \RuntimeException(Lang::get('Merchant Removed!'));
        }
    }

    /**
     * @param int $id
     * @return Merchant|Builder
     */
    public function get(int $id): Merchant|Builder
    {
        return Merchant::query()
            ->withCasts([
                'registered_at' => 'date:d.m.Y',
                'created_at'    => 'date:d.m.Y',
                'updated_at'    => 'date:d.m.Y'
            ])
            ->where('id', $id)
            ->firstOrFail();
    }
}
