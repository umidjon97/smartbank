<?php

namespace App\Http\V1\Repository;

use App\Models\Shop;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ShopRepository
{
    /**
     * @param Shop $shop
     * @return void
     */
    public function save(Shop $shop)
    {
        if (!$shop->save()) {
            throw new \RuntimeException(Lang::get('Shop save error!'));
        }
    }

    /**
     * @param int $id
     * @return Shop|Builder
     */
    public function get(int $id): Shop|Builder
    {
        return Shop::query()
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * @param string $long
     * @param string $lat
     * @return LengthAwarePaginator
     */
    public function getNearestAddress(string $long, string $lat): LengthAwarePaginator
    {
        $radius = config('app.radius');

        $long = doubleval($long);
        $lat = doubleval($lat);

        return Shop::query()
            ->select(
                DB::raw("shops.address, shops.latitude, shops.longitude,
                          ( $radius * acos( cos( radians($lat) ) *
                            cos( radians( cast(latitude as double precision) ) )
                            * cos( radians( cast(longitude as double precision) ) - radians($long)
                            ) + sin( radians($lat) ) *
                            sin( radians( cast(latitude as double precision) ) ) )
                          ) AS distance")
            )
            ->orderBy('distance')
            ->paginate(20);
    }
}
