<?php

namespace App\Http\V1\Repository;

use App\Http\V1\Helpers\UserStatusHelper;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UserRepository
{
    /**
     * @param string $username
     * @param string $status
     * @return User|Builder|null
     */
    public function findByUsernameAndStatus(
        string $username,
        string $status
    ): User|Builder|null
    {
        return User::query()
            ->where('username', $username)
            ->where('status', $status)
            ->first();
    }

    /**
     * @param string $username
     * @param string $status
     * @return Builder|User
     */
    public function getByUsernameAndStatus(
        string $username,
        string $status
    ): User|Builder
    {
        return User::query()
            ->where('username', $username)
            ->where('status', $status)
            ->firstOrFail();
    }

    /**
     * @param string $username
     * @return bool
     */
    public function isExistsUsername(string $username): bool
    {
        return User::query()
            ->where('name', $username)
            ->exists();
    }

    public function save(User $user)
    {
        if (!$user->save()) {
            throw new \RuntimeException(__('Save Error'));
        }
    }

    /**
     * @param string $token
     * @return User|null
     */
    public function findByRefreshToken(string $token): User|null
    {
        return User::query()
            ->where('refresh_token', $token)
            ->first();
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function findById(int $id): User|null
    {
        return User::query()
            ->where('id', $id)
            ->first();
    }

    /**
     * @param int $id
     * @param string $status
     * @return User|null
     */
    public function findByIdAndStatus(int $id, string $status): User|null
    {
        return User::query()
            ->where('id', $id)
            ->where('status', $status)
            ->first();
    }

    /**
     * @param int $id
     * @return User|Builder
     */
    public function getById(int $id): User|Builder
    {
        return User::query()
            ->where('id', $id)
            ->firstOrFail();
    }
}
