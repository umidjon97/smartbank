<?php

namespace App\Http\v1\Request;

use App\Exceptions\ValidationException;
use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    use ValidationException;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'latitude'  => ['required', 'string'],
            'longitude' => ['required', 'string']
        ];
    }
}
