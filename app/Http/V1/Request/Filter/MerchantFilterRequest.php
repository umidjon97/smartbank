<?php

namespace App\Http\v1\Request\Filter;

use App\Exceptions\ValidationException;
use Illuminate\Foundation\Http\FormRequest;

class MerchantFilterRequest extends FormRequest
{
    use ValidationException;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => ['string', 'nullable'],
            'status'        => ['string', 'nullable'],
            'registered_at' => ['date_format:d.m.Y', 'nullable']
        ];
    }
}
