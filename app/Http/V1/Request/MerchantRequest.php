<?php

namespace App\Http\V1\Request;

use App\Exceptions\ValidationException;
use Illuminate\Foundation\Http\FormRequest;

class MerchantRequest extends FormRequest
{
    use ValidationException;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'name'          => ['required', 'string'],
            'registered_at' => ['required', 'date_format:d.m.Y']
        ];
    }
}
