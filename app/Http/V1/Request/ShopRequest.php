<?php

namespace App\Http\v1\Request;

use App\Exceptions\ValidationException;
use App\Http\v1\Rules\MultipleColumnUniqueRule;
use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{
    use ValidationException;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'address'   => [
                'required',
                'string',
                new MultipleColumnUniqueRule($this, 'shops', ['address', 'latitude', 'longitude'])
            ],
            'latitude'  => ['required', 'integer'],
            'longitude' => ['required', 'integer']
        ];
    }
}
