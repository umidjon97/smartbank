<?php

namespace App\Http\v1\Request;

use App\Exceptions\ValidationException;
use App\Http\v1\Rules\MultipleColumnUniqueRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    use ValidationException;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * @return \string[][]
     */
    public function rules(): array
    {
        return [
            'username'    => [
                'required',
                'string',
                'min:6',
                new MultipleColumnUniqueRule($this, 'users', ['username'])
            ],
            'password'    => [
                'required',
                'min:6'
            ],
            'email'       => [
                'required',
                'email'
            ],
            'merchant_id' => [
                'required',
                'integer',
                Rule::exists('merchants', 'id'),
                Rule::unique('users', 'merchant_id')
            ],
            'permission'  => [
                'required',
                'string',
                Rule::exists('permissions', 'name')
            ],
            'guard'       => [
                'required',
                'string',
                Rule::exists('permissions', 'guard_name')
            ]
        ];
    }
}
