<?php

namespace App\Http\v1\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class MultipleColumnUniqueRule implements ValidationRule
{
    protected ?string $tableName;
    protected FormRequest $formRequest;
    protected array $columns;

    public function __construct(
        FormRequest $request,
        string      $tableName,
        array       $columns = []
    )
    {
        $this->formRequest = $request;
        $this->tableName = $tableName;
        $this->columns = $columns;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @param Closure $fail
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $queryBuilder = DB::table($this->tableName);

        foreach ($this->columns as $item) {
            $queryBuilder->where($item, $this->formRequest->{$item});
        }

        if (!empty($this->formRequest->id)) {
            $queryBuilder->whereNot('id', (int)$this->formRequest->id);
        }

        if ($queryBuilder->exists()) {
            $fail(__('validation.' . implode(', ', $this->columns) . ' already exist'));
        }
    }
}
