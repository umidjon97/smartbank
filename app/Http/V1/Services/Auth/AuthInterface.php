<?php

namespace App\Http\V1\Services\Auth;

use App\Http\v1\Request\AuthRequest;
use App\Http\v1\Request\RefreshTokenRequest;

interface AuthInterface
{
    public function login(AuthRequest $authRequest): void;

    public function refreshToken(RefreshTokenRequest $refreshTokenRequest): void;

    public function logout(): void;

    public function getAccessToken(): ?string;

    public function getRefreshToken(): ?string;
}
