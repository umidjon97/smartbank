<?php

namespace App\Http\V1\Services\Auth;

use App\Http\v1\Request\AuthRequest;
use App\Http\v1\Request\RefreshTokenRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\UnauthorizedException;

class AuthService implements AuthInterface
{
    protected ?string $access_token = null;
    protected ?string $refresh_token = null;

    /**
     * @param AuthRequest $authRequest
     * @return void
     */
    public function login(AuthRequest $authRequest): void
    {
        $this->access_token = Auth::attempt($authRequest->validated());

        if (empty($this->access_token)) {
            throw new UnauthorizedException(Lang::get('Unauthorized access'));
        }
    }

    public function refreshToken(RefreshTokenRequest $refreshTokenRequest): void
    {
        // TODO: Implement refreshToken() method.
    }

    public function logout(): void
    {
        // TODO: Implement logout() method.
    }

    /**
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->access_token;
    }

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string
    {
        return $this->refresh_token;
    }
}
