<?php

namespace App\Http\V1\Services\Links;

use App\Http\v1\Request\LinkMerchantShopRequest;

interface LinkMerchantShopInterface
{
    public function save(LinkMerchantShopRequest $merchantShopRequest): void;

    public function update(LinkMerchantShopRequest $merchantShopRequest, int $merchant_id, int $shop_id): void;
}
