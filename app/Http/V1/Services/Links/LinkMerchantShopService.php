<?php

namespace App\Http\V1\Services\Links;

use App\Http\V1\Repository\LinkMerchantShopRepository;
use App\Http\v1\Request\LinkMerchantShopRequest;
use App\Models\LinkMerchantShop;

class LinkMerchantShopService implements LinkMerchantShopInterface
{
    protected LinkMerchantShopRepository $merchantShopRepository;

    public function __construct(
        LinkMerchantShopRepository $merchantShopRepository
    )
    {
        $this->merchantShopRepository = $merchantShopRepository;
    }

    /**
     * @param LinkMerchantShopRequest $merchantShopRequest
     * @return void
     */
    public function save(LinkMerchantShopRequest $merchantShopRequest): void
    {
        $linkMerchantShop = new LinkMerchantShop();
        $linkMerchantShop->fill($merchantShopRequest->validated());
        $this->merchantShopRepository->save($linkMerchantShop);
    }

    /**
     * @param LinkMerchantShopRequest $merchantShopRequest
     * @param int $merchant_id
     * @param int $shop_id
     * @return void
     */
    public function update(LinkMerchantShopRequest $merchantShopRequest, int $merchant_id, int $shop_id): void
    {
        $linkMerchantShop = $this->merchantShopRepository->get($merchant_id, $shop_id);
        $linkMerchantShop->fill($merchantShopRequest->validated());
        $this->merchantShopRepository->save($linkMerchantShop);
    }
}
