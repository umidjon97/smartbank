<?php

namespace App\Http\V1\Services\Merchants;

use App\Http\v1\Request\Filter\MerchantFilterRequest;
use App\Http\V1\Request\MerchantRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface MerchantInterface
{
    public function list(MerchantFilterRequest $filterRequest): LengthAwarePaginator;

    public function save(MerchantRequest $merchantRequest): int;

    public function update(MerchantRequest $merchantRequest, int $id): int;

    public function delete(int $id): bool;

    public function show(int $id): Model|Builder;
}
