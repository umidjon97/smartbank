<?php

namespace App\Http\V1\Services\Merchants;

use App\Http\V1\Helpers\Db\Transaction;
use App\Http\V1\Repository\Filters\MerchantFilterRepository;
use App\Http\V1\Repository\MerchantRepository;
use App\Http\V1\Repository\UserRepository;
use App\Http\v1\Request\Filter\MerchantFilterRequest;
use App\Http\V1\Request\MerchantRequest;
use App\Models\Merchant;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Tymon\JWTAuth\Facades\JWTAuth;

class MerchantService implements MerchantInterface
{
    private User $user;

    protected MerchantFilterRepository $merchantFilterRepository;
    protected UserRepository $userRepository;
    protected MerchantRepository $merchantRepository;
    protected Transaction $transaction;

    public function __construct(
        MerchantFilterRepository $merchantFilterRepository,
        UserRepository           $userRepository,
        MerchantRepository       $merchantRepository,
        Transaction              $transaction
    )
    {
        $this->merchantFilterRepository = $merchantFilterRepository;
        $this->userRepository = $userRepository;
        $this->merchantRepository = $merchantRepository;
        $this->transaction = $transaction;

        $jwtToken = JWTAuth::parseToken();
        $id = (int)$jwtToken->getPayload()->get('sub');
        /**
         * @var User $user
         */
        $this->user = $this->userRepository->getById($id);
    }

    /**
     * @param MerchantFilterRequest $filterRequest
     * @return LengthAwarePaginator
     */
    public function list(MerchantFilterRequest $filterRequest): LengthAwarePaginator
    {
        return $this->merchantFilterRepository->filter($filterRequest, $this->user->merchant_id);
    }

    /**
     * @param MerchantRequest $merchantRequest
     * @return int
     */
    public function save(MerchantRequest $merchantRequest): int
    {
        $merchant = new Merchant();
        $merchant->fill($merchantRequest->validated());
        $this->merchantRepository->save($merchant);
        return $merchant->id;
    }

    /**
     * @param MerchantRequest $merchantRequest
     * @param int $id
     * @return int
     */
    public function update(MerchantRequest $merchantRequest, int $id): int
    {
        $merchant = $this->merchantRepository->get($id);
        $merchant->fill($merchantRequest->validated());
        $this->merchantRepository->save($merchant);
        return $merchant->id;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        if ($id !== $this->user->merchant_id) {
            abort(403, 'Access Forbidden');
        }

        $merchant = $this->merchantRepository->get($id);
        $this->merchantRepository->delete($merchant);
        return true;
    }

    /**
     * @param int $id
     * @return Merchant|Builder
     */
    public function show(int $id): Merchant|Builder
    {
        if ($id !== $this->user->merchant_id) {
            abort(403, 'Access Forbidden');
        }

        return $this->merchantRepository->get($id);
    }
}
