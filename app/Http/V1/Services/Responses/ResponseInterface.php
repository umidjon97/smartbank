<?php

namespace App\Http\V1\Services\Responses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

interface ResponseInterface
{
    public function success(string $message, array|Model $data = [], int $code = 200): JsonResponse;

    public function fail();
}
