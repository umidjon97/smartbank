<?php

namespace App\Http\V1\Services\Responses;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class ResponseService implements ResponseInterface
{
    /**
     * @param string $message
     * @param array|Model $data
     * @param int $code
     * @return JsonResponse
     */
    public function success(string $message, array|Model $data = [], int $code = 200): JsonResponse
    {
        return response()->json([
            'status'  => true,
            'message' => $message,
            'code'    => $code,
            'data'    => $data
        ]);
    }

    public function fail()
    {
        // TODO: Implement fail() method.
    }
}
