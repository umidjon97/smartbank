<?php

namespace App\Http\V1\Services\Shops;

use App\Http\v1\Request\AddressRequest;
use App\Http\v1\Request\ShopRequest;

interface ShopInterface
{
    public function save(ShopRequest $shopRequest, int $merchant_id): int;

    public function update(ShopRequest $shopRequest, int $merchant_id, int $shop_id): int;

    public function delete(int $merchant_id, int $shop_id): bool;

    public function getNearestAddress(AddressRequest $addressRequest);
}
