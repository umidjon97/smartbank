<?php

namespace App\Http\V1\Services\Shops;

use App\Http\V1\Helpers\Db\Transaction;
use App\Http\V1\Repository\LinkMerchantShopRepository;
use App\Http\V1\Repository\ShopRepository;
use App\Http\V1\Repository\UserRepository;
use App\Http\v1\Request\AddressRequest;
use App\Http\v1\Request\ShopRequest;
use App\Http\V1\Services\Links\LinkMerchantShopService;
use App\Models\LinkMerchantShop;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class ShopService implements ShopInterface
{
    protected LinkMerchantShopRepository $merchantShopRepository;
    protected ShopRepository $shopRepository;
    protected UserRepository $userRepository;
    protected Transaction $transaction;

    public function __construct(
        LinkMerchantShopRepository $merchantShopRepository,
        ShopRepository             $shopRepository,
        UserRepository             $userRepository,
        Transaction                $transaction
    )
    {
        $this->merchantShopRepository = $merchantShopRepository;
        $this->shopRepository = $shopRepository;
        $this->userRepository = $userRepository;
        $this->transaction = $transaction;
    }

    /**
     * @param ShopRequest $shopRequest
     * @param int $merchant_id
     * @return int
     */
    public function save(ShopRequest $shopRequest, int $merchant_id): int
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        if ($merchant_id !== $user->merchant_id) {
            abort(403, 'Access Forbidden');
        }

        $shop = new Shop();
        $linkMerchantShop = new LinkMerchantShop();

        $linkMerchantShop->setMerchantId($merchant_id);
        $shop->fill($shopRequest->validated());

        $this->transaction->wrap(function () use ($shop, $linkMerchantShop) {
            $this->shopRepository->save($shop);

            $linkMerchantShop->setShopId($shop->id);
            $this->merchantShopRepository->save($linkMerchantShop);
        });

        return $shop->id;
    }

    /**
     * @param ShopRequest $shopRequest
     * @param int $merchant_id
     * @param int $shop_id
     * @return int
     */
    public function update(ShopRequest $shopRequest, int $merchant_id, int $shop_id): int
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        if ($merchant_id !== $user->merchant_id) {
            abort(403, 'Access Forbidden');
        }

        $merchantShop = $this->merchantShopRepository->get($merchant_id, $shop_id);
        $shop = $merchantShop->shop;

        $merchantShop->setMerchantId($merchant_id);
        $merchantShop->setShopId($shop_id);

        $shop->fill($shopRequest->validated());

        $this->transaction->wrap(function () use ($merchantShop, $shop) {
            $this->shopRepository->save($shop);

            $this->merchantShopRepository->save($merchantShop);
        });

        return $shop->id;
    }

    public function getNearestAddress(AddressRequest $addressRequest)
    {
        $long = $addressRequest->post('longitude');
        $lat = $addressRequest->post('latitude');

        return $this->shopRepository->getNearestAddress($long, $lat);
    }

    /**
     * @param int $merchant_id
     * @param int $shop_id
     * @return bool
     */
    public function delete(int $merchant_id, int $shop_id): bool
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        if ($merchant_id !== $user->merchant_id) {
            abort(403, 'Access Forbidden');
        }

        $merchantShop = $this->merchantShopRepository->get($merchant_id, $shop_id);
        $this->merchantShopRepository->delete($merchantShop);
        return true;
    }
}
