<?php

namespace App\Http\V1\Services\Users;

use App\Http\v1\Request\UserRequest;

interface UserInterface
{
    public function save(UserRequest $userRequest): int;

    public function update(UserRequest $userRequest, int $id): int;

    public function destroy(int $id): bool;
}
