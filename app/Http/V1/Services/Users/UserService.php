<?php

namespace App\Http\V1\Services\Users;

use App\Http\V1\Helpers\Db\Transaction;
use App\Http\V1\Repository\UserRepository;
use App\Http\v1\Request\UserRequest;
use App\Models\User;

class UserService implements UserInterface
{
    protected Transaction $transaction;
    protected UserRepository $userRepository;

    public function __construct(
        Transaction    $transaction,
        UserRepository $userRepository
    )
    {
        $this->transaction = $transaction;
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserRequest $userRequest
     * @return int
     */
    public function save(UserRequest $userRequest): int
    {
        $user = new User();
        $user->fill($userRequest->only(['username', 'password', 'email', 'merchant_id']));

        $this->transaction->wrap(function () use ($user, $userRequest) {
            $this->userRepository->save($user);

            $user->syncPermissions([$userRequest->only(['permission'])]);
        });

        return $user->id;
    }

    public function update(UserRequest $userRequest, int $id): int
    {
        // TODO: Implement update() method.
    }

    public function destroy(int $id): bool
    {
        // TODO: Implement destroy() method.
    }
}
