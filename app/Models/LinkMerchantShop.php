<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $merchant_id
 * @property int $shop_id
 * @property string $status
 *
 * @property-read Shop $shop
 */
class LinkMerchantShop extends Model
{
    protected $primaryKey = 'merchant_id';

    protected $fillable = [
        'merchant_id',
        'shop_id',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @param int $merchant_id
     * @return void
     */
    public function setMerchantId(int $merchant_id): void
    {
        $this->merchant_id = $merchant_id;
    }

    /**
     * @param int $shop_id
     * @return void
     */
    public function setShopId(int $shop_id): void
    {
        $this->shop_id = $shop_id;
    }
}
