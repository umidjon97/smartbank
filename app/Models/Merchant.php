<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $name
 * @property string $registered_at
 * @property string $status
 */
class Merchant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'registered_at',
        'status'
    ];

    /**
     * @return HasMany
     */
    public function linkShops(): HasMany
    {
        return $this->hasMany(LinkMerchantShop::class, 'merchant_id', 'id');
    }
}
