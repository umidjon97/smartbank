<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $address
 * @property string $status
 * @property int $latitude
 * @property int $longitude
 */
class Shop extends Model
{
    protected $fillable = [
        'address',
        'latitude',
        'longitude',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function merchant(): BelongsTo
    {
        return $this->belongsTo(Merchant::class);
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @param int $latitude
     */
    public function setLatitude(int $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @param int $longitude
     */
    public function setLongitude(int $longitude): void
    {
        $this->longitude = $longitude;
    }
}
