<?php

namespace App\Providers\Auth;

use App\Http\V1\Helpers\UserStatusHelper;
use App\Http\V1\Repository\UserRepository;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

class UsernamePasswordUserProvider implements UserProvider
{
    protected UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param $identifier
     * @return Builder|User|Authenticatable|null
     */
    public function retrieveById($identifier): Builder|User|Authenticatable|null
    {
        $status = UserStatusHelper::_ACTIVE->value;

        return $this->userRepository->findByIdAndStatus($identifier, $status);
    }

    /**
     * @param $identifier
     * @param $token
     * @return void
     * @throws Exception
     */
    public function retrieveByToken($identifier, $token)
    {
        throw new Exception('Method not implemented.');
    }

    /**
     * @param Authenticatable $user
     * @param $token
     * @return void
     * @throws Exception
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        throw new Exception('Method not implemented.');
    }

    /**
     * @param array $credentials
     * @return bool|Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials): bool|Authenticatable|null
    {
        $username = (string)$credentials['username'];
        $password = (string)$credentials['password'];

        $status = UserStatusHelper::_ACTIVE->value;

        /**
         * @var User $user
         */
        $user = $this->userRepository->getByUsernameAndStatus($username, $status);

        if (Hash::check($password, $user->password)) {
            return $user;
        }

        throw new UnauthorizedException(__('auth.Username or Password error!'));

    }

    /**
     * @param Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        $username = $credentials['username'];
        $password = $credentials['password'];

        $status = UserStatusHelper::_ACTIVE->value;

        /**
         * @var User $user
         */
        $user = $this->userRepository->getByUsernameAndStatus($username, $status);

        return Hash::check($password, $user->password);
    }
}
