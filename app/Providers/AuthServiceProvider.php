<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Http\V1\Repository\UserRepository;
use App\Providers\Auth\UsernamePasswordUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Auth::provider('username_password', function ($app, $config) {
            return new UsernamePasswordUserProvider(new UserRepository());
        });
    }
}
