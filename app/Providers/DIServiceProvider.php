<?php

namespace App\Providers;

use App\Http\V1\Services\Auth\AuthInterface;
use App\Http\V1\Services\Auth\AuthService;
use App\Http\V1\Services\Merchants\MerchantInterface;
use App\Http\V1\Services\Merchants\MerchantService;
use App\Http\V1\Services\Responses\ResponseInterface;
use App\Http\V1\Services\Responses\ResponseService;
use App\Http\V1\Services\Shops\ShopInterface;
use App\Http\V1\Services\Shops\ShopService;
use App\Http\V1\Services\Users\UserInterface;
use App\Http\V1\Services\Users\UserService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class DIServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(MerchantInterface::class, MerchantService::class);

        $this->app->bind(ResponseInterface::class, ResponseService::class);

        $this->app->bind(ShopInterface::class, ShopService::class);

        $this->app->bind(AuthInterface::class, AuthService::class);

        $this->app->bind(UserInterface::class, UserService::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            MerchantInterface::class,
            ResponseInterface::class,
            ShopInterface::class,
            AuthInterface::class,
            UserInterface::class
        ];
    }
}
