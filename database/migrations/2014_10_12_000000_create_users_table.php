<?php

use App\Http\V1\Helpers\UserStatusHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $allowed = [
            UserStatusHelper::_ACTIVE->value,
            UserStatusHelper::_NO_ACTIVE->value,
            UserStatusHelper::_BAN->value,
        ];

        Schema::create('users', function (Blueprint $table) use ($allowed) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('status', $allowed)->default(UserStatusHelper::_ACTIVE->value);
            $table->string('refresh_token')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->index('username', 'idx-users-username');
            $table->index('status', 'idx-users-status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('idx-users-username');
            $table->dropIndex('idx-users-status');
        });

        Schema::dropIfExists('users');
    }
};
