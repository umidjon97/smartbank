<?php

use App\Http\V1\Helpers\MerchantStatusHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $allowed = [
            MerchantStatusHelper::_ACTIVE->value,
            MerchantStatusHelper::_BAN->value,
        ];

        Schema::create('merchants', function (Blueprint $table) use ($allowed) {
            $table->id();
            $table->string('name');
            $table->enum('status', $allowed)->default(MerchantStatusHelper::_ACTIVE->value);
            $table->date('registered_at');
            $table->timestamps();

            $table->index('name', 'idx-merchants-name');
            $table->index('status', 'idx-merchants-status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropIndex('idx-merchants-status');
            $table->dropIndex('idx-merchants-name');
        });

        Schema::dropIfExists('merchants');
    }
};
