<?php

use App\Http\V1\Helpers\ShopStatusHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $allowed = [
            ShopStatusHelper::_ACTIVE->value,
            ShopStatusHelper::_NO_ACTIVE->value
        ];

        Schema::create('shops', function (Blueprint $table) use ($allowed) {
            $table->id();
            $table->string('address');
            $table->bigInteger('latitude');
            $table->bigInteger('longitude');
            $table->enum('status', $allowed)->default(ShopStatusHelper::_ACTIVE->value);
            $table->timestamps();

            $table->index('address', 'idx-shops-address');
            $table->index('latitude', 'idx-shops-latitude');
            $table->index('longitude', 'idx-shops-longitude');

            $table->unique(['address', 'latitude', 'longitude']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('shops', function (Blueprint $table) {
            $table->dropUnique('shops_address_latitude_longitude_unique');
            $table->dropIndex('idx-shops-longitude');
            $table->dropIndex('idx-shops-latitude');
            $table->dropIndex('idx-shops-address');
        });

        Schema::dropIfExists('shops');
    }
};
