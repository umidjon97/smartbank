<?php

use App\Http\V1\Helpers\LinkMerchantShopStatusHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $allowed = [
            LinkMerchantShopStatusHelper::_ACTIVE->value,
            LinkMerchantShopStatusHelper::_NO_ACTIVE->value,
        ];

        Schema::create('link_merchant_shops', function (Blueprint $table) use ($allowed) {
            $table->integer('merchant_id');
            $table->integer('shop_id');
            $table->enum('status', $allowed)->default(LinkMerchantShopStatusHelper::_ACTIVE->value);
            $table->timestamps();

            $table->foreign('merchant_id')
                ->references('id')
                ->on('merchants')
                ->cascadeOnDelete();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->cascadeOnDelete();

            $table->index(['merchant_id', 'shop_id'], 'idx-link_merchant_shops-merchant_id-shop_id');
            $table->primary(['merchant_id', 'shop_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('link_merchant_shops', function (Blueprint $table) {
            $table->dropIndex('idx-link_merchant_shops-merchant_id-shop_id');

            $table->dropForeign('link_merchant_shops_shop_id_foreign');
            $table->dropForeign('link_merchant_shops_merchant_id_foreign');
        });

        Schema::dropIfExists('link_merchant_shops');
    }
};
