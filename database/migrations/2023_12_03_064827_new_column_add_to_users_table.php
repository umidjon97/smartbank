<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('merchant_id')->nullable();

            $table->foreign('merchant_id')
                ->references('id')
                ->on('merchants');

            $table->index('merchant_id', 'idx-users-merchant_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('idx-users-merchant_id');

            $table->dropForeign('users_merchant_id_foreign');

            $table->dropColumn('merchant_id');
        });
    }
};
