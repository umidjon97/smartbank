<?php

namespace Database\Seeders;

use App\Http\V1\Repository\UserRepository;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AddUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            'username' => 'smartbank',
            'email'    => 'smartbank@mail.ru',
            'password' => Hash::make('Qwerty123$')
        ];

        if (User::query()->where('username', 'smartbank')->exists()) {
            return;
        }

        $user = new User();
        $user->fill($users);
        App::make(UserRepository::class)->save($user);

        echo 'Done...';
    }
}
