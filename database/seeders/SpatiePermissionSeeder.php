<?php

namespace Database\Seeders;

use App\Http\V1\Helpers\PermissionHelper;
use App\Http\V1\Helpers\UserStatusHelper;
use App\Http\V1\Repository\UserRepository;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;

class SpatiePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        /**
         * @var UserRepository $userRepository
         */
        $userRepository = App::make(UserRepository::class);

        if (Permission::query()->where('name', PermissionHelper::_MANAGER->value)->exists()) {
            return;
        }

        Permission::create(['name' => PermissionHelper::_MODERATOR->value, 'guard_name' => 'api']);
        Permission::create(['name' => PermissionHelper::_MANAGER->value, 'guard_name' => 'api']);

        $user = $userRepository->getByUsernameAndStatus('smartbank', UserStatusHelper::_ACTIVE->value);
        $user->givePermissionTo(PermissionHelper::_MODERATOR->value);
    }
}
