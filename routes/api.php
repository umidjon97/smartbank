<?php

use App\Http\V1\Controllers\AuthController;
use App\Http\V1\Controllers\MerchantController;
use App\Http\V1\Controllers\ShopController;
use App\Http\V1\Controllers\UserController;
use App\Http\V1\Helpers\PermissionHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(MerchantController::class)
    ->middleware(['auth-jwt', 'permission:' . PermissionHelper::_MANAGER->value])
    ->group(function () {
        Route::get('merchant/list', 'index');

        Route::get('merchant/view/{id}', 'show');

        Route::delete('merchant/delete/{id}', 'destroy');

        Route::post('merchant/save', 'store')
            ->withoutMiddleware(['permission:' . PermissionHelper::_MANAGER->value])
            ->middleware(['permission:' . PermissionHelper::_MODERATOR->value]);

        Route::put('merchant/update/{id}', 'update')
            ->withoutMiddleware(['permission:' . PermissionHelper::_MANAGER->value])
            ->middleware(['permission:' . PermissionHelper::_MODERATOR->value]);
    });

Route::controller(ShopController::class)
    ->middleware(['auth-jwt'])
    ->group(function () {
        Route::post('shop/save/{merchant_id}', 'store')
            ->middleware(['permission:' . PermissionHelper::_MANAGER->value]);

        Route::put('shop/update/{merchant_id}/{shop_id}', 'update')
            ->middleware(['permission:' . PermissionHelper::_MANAGER->value]);

        Route::delete('shop/delete/{merchant_id}/{shop_id}', 'destroy')
            ->middleware(['permission:' . PermissionHelper::_MANAGER->value]);

        Route::post('shop/nearest-address', 'nearestAddress')
            ->withoutMiddleware(['auth-jwt']);
    });

Route::controller(AuthController::class)
    ->group(function () {
        Route::post('auth/login', 'login');
    });

Route::controller(UserController::class)
    ->middleware(['auth-jwt'])
    ->group(function () {
        Route::post('user/create', 'store')
            ->middleware(['permission:' . PermissionHelper::_MODERATOR->value])
            ->name('user.create');

        Route::put('user/update/{id}', 'update')
            ->middleware(['permission:' . PermissionHelper::_MODERATOR->value])
            ->name('user.update');

        Route::delete('user/delete/{id}', 'destroy')
            ->middleware(['permission:' . PermissionHelper::_MODERATOR->value])
            ->name('user.destroy');
    });

