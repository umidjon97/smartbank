<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {

    $status = opcache_get_status();

    echo 'Cached scripts: ' . $status['opcache_statistics']['num_cached_scripts'] . PHP_EOL;
    echo 'Cache hits: ' . $status['opcache_statistics']['hits'] . PHP_EOL;
    echo 'Cache misses: ' . $status['opcache_statistics']['misses'] . PHP_EOL;
    echo 'Memory usage: ' . $status['memory_usage']['used_memory'] / (1024 * 1024) . ' MB' . PHP_EOL;

//    return view('welcome');
});
